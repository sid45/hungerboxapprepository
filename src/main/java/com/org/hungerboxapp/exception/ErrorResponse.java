package com.org.hungerboxapp.exception;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorResponse {

	private List<String> message;
	private int status;

}
