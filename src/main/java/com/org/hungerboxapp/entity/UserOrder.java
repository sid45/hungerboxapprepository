package com.org.hungerboxapp.entity;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class UserOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long userOderId;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserDetail user;

	private String totalPrice;

	private double orderDate;

}
